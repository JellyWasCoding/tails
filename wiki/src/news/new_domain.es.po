# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-07-27 18:45+0000\n"
"PO-Revision-Date: 2024-01-30 14:58+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta  title=\"Welcome to tails.net!\"]]\n"
msgstr "[[!meta title=\"¡Bienvenidos a tails.net!\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta  date=\"Thu, 27 Jul 2023 17:53:31 +0000\"]]\n"
msgstr "[[!meta  date=\"Thu, 27 Jul 2023 17:53:31 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"Today, we finished migrating our website to a new domain: [[tails.net|"
"https://tails.net]]."
msgstr ""
"Hoy hemos terminado de migrar nuestro sitio web a un nuevo dominio: [[tails."
"net|https://tails.net]]."

#. type: Plain text
msgid ""
"After 14 years using a subdomain of `boum.org`, we finally found a new home "
"that will be easier for everybody to remember and type. Tails will become "
"easier to find by the people who need it the most."
msgstr ""
"Después de 14 años utilizando un subdominio de `boum.org`, por fin hemos "
"encontrado un nuevo hogar que será más fácil de recordar y teclear para todo "
"el mundo. Tails será más fácil de encontrar para las personas que más lo "
"necesitan."

#. type: Plain text
msgid ""
"The new domain also provides us more autonomy and possibilities for some "
"technical work."
msgstr ""
"El nuevo dominio también nos proporciona más autonomía y posibilidades para "
"algunos trabajos técnicos."

#. type: Plain text
msgid ""
"The former `tails.boum.org` domain will continue working until further "
"notice, to avoid breaking upgrades and security notifications on older "
"versions of Tails and external links on the Internet."
msgstr ""
"El antiguo dominio `tails.boum.org` seguirá funcionando hasta nuevo aviso, "
"para evitar que se rompan las actualizaciones y notificaciones de seguridad "
"en versiones antiguas de Tails y enlaces externos en Internet."

#. type: Plain text
msgid ""
"Please make sure to update any important pointers you might have or know "
"about."
msgstr ""
"Por favor, asegúrate de actualizar cualquier enlace importante que puedas "
"tener o conocer."

#. type: Plain text
msgid ""
"We want to thank `boum.org` for supporting the project for all these years.  "
"Their support was and continues to be very valuable! :)"
msgstr ""
"Queremos dar las gracias a `boum.org` por apoyar el proyecto durante todos "
"estos años. Su apoyo ha sido y sigue siendo muy valioso. :)"
