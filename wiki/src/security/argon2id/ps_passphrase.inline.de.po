# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-09-04 09:56+0200\n"
"PO-Revision-Date: 2023-11-28 11:42+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Bullet: '   1. '
msgid "Choose **Applications**&nbsp;▸ **Persistent Storage**."
msgstr "Wählen Sie **Anwendungen**&nbsp;▸ **Persistenter Speicher**."

#. type: Bullet: '   1. '
msgid "Click on the **Change Passphrase** button on the left of the title bar."
msgstr ""
"Klicken Sie auf die Schaltfläche **Passphrase ändern** auf der linken Seite "
"der Titelleiste."

#. type: Bullet: '   1. '
msgid "Enter the current passphrase in the **Current Passphrase** text box."
msgstr ""
"Geben Sie die aktuelle Passphrase in das Textfeld **Aktuelle Passphrase** "
"ein."

#. type: Bullet: '   1. '
msgid "Enter your new passphrase in the **New Passphrase** text box."
msgstr "Geben Sie Ihre neue Passphrase in das Textfeld **Neue Passphrase** ein."

#. type: Bullet: '   1. '
msgid ""
"Enter your new passphrase again in the **Confirm New Passphrase** text box."
msgstr ""
"Geben Sie Ihre neue Passphrase erneut in das Textfeld **Neue Passphrase "
"bestätigen** ein."

#. type: Bullet: '   1. '
msgid "Click **Change**."
msgstr ""

#. type: Bullet: '   1. '
msgid "Close the **Persistent Storage** settings."
msgstr ""
