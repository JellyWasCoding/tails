# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-13 00:13+0000\n"
"PO-Revision-Date: 2023-11-24 10:43+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Chatting with Dino and OMEMO\"]]\n"
msgstr "[[!meta title=\"Plaudern mit Dino und OMEMO\"]]\n"

#. type: Plain text
msgid ""
"[*Dino*](https://dino.im/) is a modern, open-source Jabber/XMPP chat client. "
"*Dino* supports end-to-end encryption using [OMEMO](https://conversations.im/"
"omemo/)."
msgstr ""
"[*Dino*](https://dino.im/) ist ein moderner, quelloffener Jabber/XMPP-Chat-"
"Client. *Dino* unterstützt Ende-zu-Ende-Verschlüsselung mit "
"[OMEMO](https://conversations.im/omemo/)."

#. type: Plain text
msgid ""
"To learn how to install and use *Dino* in Tails, see [this tutorial](https://"
"t-hinrichs.net/DinoTails/DinoTails_recent.html)."
msgstr ""
"Um zu erfahren, wie man *Dino* in Tails installiert und benutzt, siehe ["
"diese Anleitung] (https://t-hinrichs.net/DinoTails/DinoTails_recent.html)."

#. type: Plain text
#, no-wrap
msgid "<div class=\"bug\">\n"
msgstr "<div class=\"bug\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>The tutorial linked above is outdated since Tails 5.0 (May 2022). See [[!tails_ticket 19104]].</p>\n"
msgstr ""
"<p>Das oben verlinkte Tutorial ist seit Tails 5.0 (Mai 2022) veraltet. Siehe "
"[[!tails_ticket 19104]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
msgid ""
"We are considering *Dino* as an option to replace [[*Pidgin*|doc/"
"anonymous_internet/pidgin]] in Tails."
msgstr ""
