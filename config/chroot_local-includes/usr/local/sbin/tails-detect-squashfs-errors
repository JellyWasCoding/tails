#!/usr/bin/env python3

"""
  Detect SQUASHFS error and if detected create
  /var/lib/live/tails.squashfs_failed. That file will be used to show
  the user an error.

  Copyright (C) 2023 Tails developers <tails@boum.org>

  You can redistribute  it and/or modify it under the  terms of the GNU
  General Public License as published by the Free Software Foundation;
  either version 3 of the License, or (at your option) any later version.

  This program  is distributed in the  hope that it will  be useful, but
  WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
  MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pathlib

import systemd.daemon
import systemd.journal

# File that indicates SQUASHFS errors
squashfs_failed = pathlib.Path("/var/lib/live/tails.squashfs_failed")

def process_entries(journal: systemd.journal.Reader):
    """Process all journald messages and search for SQUASHFS error."""
    for e in journal:
        if e["MESSAGE"].startswith("SQUASHFS error:"):
            print(e["MESSAGE"])     # Add detected logs to own log
            squashfs_failed.touch(exist_ok=True)


def main():
    flags = systemd.journal.SYSTEM_ONLY | systemd.journal.LOCAL_ONLY
    j = systemd.journal.Reader(flags)
    j.this_boot()
    j.this_machine()
    j.log_level(systemd.journal.LOG_ERR)
    j.add_match(SYSLOG_IDENTIFIER="kernel")

    # On start we havn't detected any error
    squashfs_failed.unlink(missing_ok=True)

    systemd.daemon.notify('STATUS=Processing existing messages...\n')

    #Process all existing log entries since boot
    process_entries(j)

    # Notify systemd that we're ready
    systemd.daemon.notify('READY=1')
    systemd.daemon.notify('STATUS=Waiting for new messages to process...\n')

    # Wait for new messages to be appended to journald
    while True:
        state_change = j.wait()
        if state_change == systemd.journal.APPEND:
            process_entries(j)


if __name__ == '__main__':
    main()
